import 'dart:html';
import 'dart:async';

void main() {
  InputElement colorInput = querySelector("#color");
  InputElement percentageInput = querySelector("#percentage");
  Element colorExample = querySelector('#color-example');
  colorInput.onInput.listen((e) {
    colorExample.style.backgroundColor = colorInput.value;
  });
  colorExample.style.backgroundColor = colorInput.value = "rgb(0,174,239)";
  ButtonElement generate = querySelector("#generate");
  InputElement fileInput = querySelector("#file");
  generate.onClick.listen((event) {
    File file = fileInput.files.first;
    new FileReader()
        ..onLoad.listen((value) {
          var images = querySelector("#images");
          images.children.clear();
          [
            new Resolution('xlarge', 720, 960),
            new Resolution('large', 480, 640),
            new Resolution('normal', 320, 470),
            new Resolution('small', 320, 426),
            new Resolution('xlarge-horizontal', 960, 720),
            new Resolution('large-horizontal', 640, 480),
            new Resolution('normal-horizontal', 470, 320),
            new Resolution('small-horizontal', 426, 320)
          ].forEach((Resolution res) {
            NinePatchImage(value.target.result, res.width, res.height, percentageInput.valueAsNumber, colorInput.value).then((dataUrl) {
              var newFilename = "${getFilenameWithoutExtension(file.name)}-${res.name}.9.png";
              var download = new Element.html('<p><a>download</a> $newFilename (${res.width}x${res.height})</p>');
              AnchorElement a = download.querySelector("a");
              a.href = dataUrl;
              a.attributes["download"] = newFilename;
              images.children.add(new DivElement()
                ..children.add(download)
                ..children.add(new ImageElement()
                  ..src = dataUrl
                  ..width = (res.width / 4).round()
                  ..height = (res.height / 4).round()
                )
              );
            });
          });
        })
        ..readAsDataUrl(file);
  });
  
  
}

class Resolution {
  int width, height;
  String name;
  Resolution(this.name, this.width, this.height);
}

String getFilenameWithoutExtension(String name) {
  var regExp = new RegExp(r"^(.+?)(\.[a-z]+)?$", caseSensitive: false);
  return regExp.firstMatch(name)[1];
}

ImageData createBlackPixel(CanvasRenderingContext2D context) {
  return context.createImageData(1, 1)
      ..data[0] = 0
      ..data[1] = 0
      ..data[2] = 0
      ..data[3] = 255;
}

Future<String> NinePatchImage(String url, int width, int height, num percentage, String backgroundColor) {
  var image = new ImageElement();
  image.src = url;
  return image.onLoad.first.then((event) {
    var canvas = new CanvasElement(width: width + 2, height: height + 2);
    var context = canvas.context2D;
    context.fillStyle = backgroundColor;
    context.fillRect(0, 0, canvas.width, canvas.height);
    num contentWidth = width * percentage;
    num contentHeight = height * percentage;
    num widthRatio = contentWidth / image.width;
    num heightRatio = contentHeight / image.height;
    num ratio = widthRatio < heightRatio
        ? widthRatio
        : heightRatio;
    int newWidth = (image.width * ratio).round();
    int newHeight = (image.height * ratio).round();
    int newX = ((width - newWidth) / 2 + 1).round();
    int newY = ((height - newHeight) / 2 + 1).round();
    context.drawImageScaled(image, newX, newY, newWidth, newHeight);
    var pixel = createBlackPixel(context);
    
    // draw patches
    for (int x = 1; x < newX; x++) {
      context.putImageData(pixel, x, 0);
    }
    for (int x = newX + newWidth; x < canvas.width - 1; x++) {
      context.putImageData(pixel, x, 0);
    }
    for (int y = 1; y < newY; y++) {
      context.putImageData(pixel, 0, y);
    }
    for (int y = newY + newHeight; y < canvas.width - 1; y++) {
      context.putImageData(pixel, 0, y);
    }
    return canvas.toDataUrl('image/png');
  });
}
